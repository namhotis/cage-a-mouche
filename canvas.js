function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

window.addEventListener('load', function () {
    var canvas = document.getElementById('mon_canvas');
    if (!canvas) {
        alert("Impossible de récupérer le canvas");
        return;
    }

    var context = canvas.getContext('2d');
    if (!context) {
        alert("Impossible de récupérer le context du canvas");
        return;
    }

    canvas.height = 500;
    canvas.width = 300;

    var particles = [];
    for (var i = 0; i < 10; i++) {
        var a = getRndInteger(-5, 5);
        var b = getRndInteger(-5, 5);
        particles.push(new Particle({
            size: 5,
            speedX: a,
            speedY: b,
            x: 100,
            y: 100,
            context: context
        }));
    }

    document.getElementById("mon_canvas").addEventListener("click", addMouche);

    function addMouche() {
        var x = event.clientX;
        var y = event.clientY;
        particles.push(new Particle({
            size: 5,
            speedX: a,
            speedY: b,
            x: event.clientX,
            y: event.clientY,
            context: context
        }));
    }




    animate();

    function animate() {
        //Clear de la frame
        requestAnimationFrame(animate);
        context.clearRect(0, 0, canvas.width, canvas.height);

        particles.forEach(particle => {
            particle.move();

        })
    }


});