class Particle {
    constructor(params) {
        this.tailleBalle = params.size || 5;
        this.speedX = params.speedX || 0;
        this.speedY = params.speedY || 0;
        this.context = params.context;
        this.x = params.x;
        this.y = params.y;
        var r = getRndInteger(0, 255);
        var g = getRndInteger(0, 255);
        var b = getRndInteger(0, 255);
        this.color = "rgb(" + r + ", " + g + ", " + b + ")";
    }

    move() {
        this.x += this.speedX;
        this.y += this.speedY;
        this.draw();

        var a = getRndInteger(-1, 1);
        var b = getRndInteger(-1, 1);
        this.speedX += a;
        this.speedY += b;


        //A l'avenir, utiliser case

        if (this.x <= this.tailleBalle) {
            this.speedX *= -1;
            this.x = 1 + this.tailleBalle;
        }

        if (this.x >= 300 - this.tailleBalle) {
            this.speedX *= -1;
            this.x = 300 - this.tailleBalle;
        }

        if (this.y >= 500 - this.tailleBalle) {
            this.speedY *= -1;
            this.y = 500 - this.tailleBalle;
        }

        if (this.y <= this.tailleBalle) {
            this.speedY *= -1;
            this.y = 1 + this.tailleBalle;
        }

        if (this.speedX >= 10) {
            this.speedX /= 5;
        }

        if (this.speedY >= 10) {
            this.speedY /= 5;
        }

    }

    draw() {
        this.context.beginPath();
        this.context.fillStyle = this.color;

        this.context.shadowColor = this.color;
        this.context.shadowBlur = 20;
        this.context.arc(this.x, this.y, this.tailleBalle, 0, Math.PI * 2);
        this.context.fill();
        this.context.closePath();
    }
}